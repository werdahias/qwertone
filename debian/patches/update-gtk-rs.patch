From: Andrej Shadura <andrew.shadura@collabora.co.uk>
Date: Thu, 22 Feb 2024 16:14:06 +0100
Subject: Update gtk-rs

Signed-off-by: Andrej Shadura <andrew.shadura@collabora.co.uk>
---
 src/ui/menu.rs     | 25 +++++++++++++------------
 src/ui/mod.rs      | 13 +++++++------
 src/ui/timeline.rs | 52 +++++++++++++++++++++++++++-------------------------
 4 files changed, 48 insertions(+), 44 deletions(-)

diff --git a/src/ui/menu.rs b/src/ui/menu.rs
index 23ea5d3..bf97e84 100644
--- a/src/ui/menu.rs
+++ b/src/ui/menu.rs
@@ -1,6 +1,7 @@
 use std::collections::HashMap;
 use std::sync::{Arc, Mutex};
 
+use gtk::glib::Propagation;
 use gtk::prelude::*;
 
 use super::model;
@@ -20,14 +21,14 @@ impl Menu {
         let menu_pressed_fade_in = {
             let container_vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
             let switch_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
-            let fade_switch = gtk::SwitchBuilder::new()
+            let fade_switch = gtk::Switch::builder()
                 .active(*model_lock.pressed_fade_in_enabled.get())
                 .build();
             switch_hbox.pack_start(&fade_switch, false, false, 0);
             switch_hbox.pack_start(&gtk::Label::new(Some("Fade in")), false, false, 0);
 
             let value = *model_lock.pressed_fade_in_duration.get() as f64;
-            let duration_slider = gtk::ScaleBuilder::new()
+            let duration_slider = gtk::Scale::builder()
                 .adjustment(&gtk::Adjustment::new(value, 0.1, 1.0, 0.1, 0.5, 0.0))
                 .digits(1)
                 .build();
@@ -50,7 +51,7 @@ impl Menu {
                 model_lock.pressed_fade_in_enabled.set(enabled);
                 model_lock.commit();
                 slider_refdup.set_visible(enabled);
-                Inhibit(false)
+                Propagation::Proceed
             });
             container_vbox.show_all();
             duration_slider.set_visible(fade_switch.is_active());
@@ -61,7 +62,7 @@ impl Menu {
         let menu_pressed_fade_out = {
             let container_vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
             let switch_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
-            let fade_switch = gtk::SwitchBuilder::new()
+            let fade_switch = gtk::Switch::builder()
                 .active(*model_lock.pressed_fade_out_enabled.get())
                 .build();
             switch_hbox.pack_start(&fade_switch, false, false, 0);
@@ -73,7 +74,7 @@ impl Menu {
             );
 
             let value = *model_lock.pressed_fade_out_duration.get() as f64;
-            let duration_slider = gtk::ScaleBuilder::new()
+            let duration_slider = gtk::Scale::builder()
                 .adjustment(&gtk::Adjustment::new(value, 0.1, 10.0, 0.1, 1.0, 0.0))
                 .digits(1)
                 .build();
@@ -96,7 +97,7 @@ impl Menu {
                 model_lock.pressed_fade_out_enabled.set(enabled);
                 model_lock.commit();
                 slider_refdup.set_visible(enabled);
-                Inhibit(false)
+                Propagation::Proceed
             });
             container_vbox.show_all();
             duration_slider.set_visible(fade_switch.is_active());
@@ -107,7 +108,7 @@ impl Menu {
         let menu_released_fade_out = {
             let container_vbox = gtk::Box::new(gtk::Orientation::Vertical, 5);
             let switch_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
-            let fade_switch = gtk::SwitchBuilder::new()
+            let fade_switch = gtk::Switch::builder()
                 .active(*model_lock.released_fade_out_enabled.get())
                 .build();
             switch_hbox.pack_start(&fade_switch, false, false, 0);
@@ -119,7 +120,7 @@ impl Menu {
             );
 
             let value = *model_lock.released_fade_out_duration.get() as f64;
-            let duration_slider = gtk::ScaleBuilder::new()
+            let duration_slider = gtk::Scale::builder()
                 .adjustment(&gtk::Adjustment::new(value, 0.1, 10.0, 0.1, 1.0, 0.0))
                 .digits(1)
                 .build();
@@ -142,7 +143,7 @@ impl Menu {
                 model_lock.released_fade_out_enabled.set(enabled);
                 model_lock.commit();
                 slider_refdup.set_visible(enabled);
-                Inhibit(false)
+                Propagation::Proceed
             });
             container_vbox.show_all();
             duration_slider.set_visible(fade_switch.is_active());
@@ -152,7 +153,7 @@ impl Menu {
         // Metronome switch
         let menu_metronome_switch = {
             let container_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
-            let metronome_switch = gtk::SwitchBuilder::new()
+            let metronome_switch = gtk::Switch::builder()
                 .active(*model_lock.metronome_enabled.get())
                 .build();
             container_hbox.pack_start(&metronome_switch, false, false, 0);
@@ -163,7 +164,7 @@ impl Menu {
                 let mut model_lock = model_refdup.lock().unwrap();
                 model_lock.metronome_enabled.set(enabled);
                 model_lock.commit();
-                Inhibit(false)
+                Propagation::Proceed
             });
             container_hbox.show_all();
             container_hbox
@@ -172,7 +173,7 @@ impl Menu {
         // BPM spin button
         let menu_metronome_bpm = {
             let container_hbox = gtk::Box::new(gtk::Orientation::Horizontal, 5);
-            let bpm_input = gtk::SpinButtonBuilder::new()
+            let bpm_input = gtk::SpinButton::builder()
                 .adjustment(&gtk::Adjustment::new(30.0, 30.0, 480.0, 1.0, 10.0, 10.0))
                 .value(*model_lock.metronome_bpm.get() as f64)
                 .digits(0)
diff --git a/src/ui/mod.rs b/src/ui/mod.rs
index b2bc3b8..964949c 100644
--- a/src/ui/mod.rs
+++ b/src/ui/mod.rs
@@ -4,6 +4,7 @@ pub mod timeline;
 
 mod model;
 
+use gtk::glib::Propagation;
 use gtk::prelude::*;
 use std::sync::Arc;
 
@@ -12,20 +13,20 @@ use crate::globals;
 
 pub fn init(app: &gtk::Application, audio_context: Arc<crate::AudioContext>) {
     // -- Window and header ---------------------------------------------------
-    let header = gtk::HeaderBarBuilder::new()
+    let header = gtk::HeaderBar::builder()
         .title(globals::APP_NAME)
         .has_subtitle(false)
         .show_close_button(true)
         .build();
 
-    let content = gtk::GridBuilder::new()
+    let content = gtk::Grid::builder()
         .border_width(5)
         .row_spacing(5)
         .column_homogeneous(true)
         .row_homogeneous(true)
         .build();
 
-    let win = gtk::ApplicationWindowBuilder::new()
+    let win = gtk::ApplicationWindow::builder()
         .application(app)
         .title(globals::APP_NAME)
         .default_width(800)
@@ -50,7 +51,7 @@ pub fn init(app: &gtk::Application, audio_context: Arc<crate::AudioContext>) {
 
     // -- CSS loading ---------------------------------------------------------
     let css = include_str!("../../res/style.css");
-    let screen = win.screen().unwrap();
+    let screen = GtkWindowExt::screen(&win).unwrap();
     let style_provider = gtk::CssProvider::new();
     style_provider.load_from_data(css.as_bytes()).unwrap();
     gtk::StyleContext::add_provider_for_screen(
@@ -89,7 +90,7 @@ pub fn init(app: &gtk::Application, audio_context: Arc<crate::AudioContext>) {
                 }
             }
         }
-        Inhibit(false)
+        Propagation::Proceed
     });
 
     // Key release
@@ -114,7 +115,7 @@ pub fn init(app: &gtk::Application, audio_context: Arc<crate::AudioContext>) {
                 }
             }
         }
-        Inhibit(false)
+        Propagation::Proceed
     });
 
     win.show_all();
diff --git a/src/ui/timeline.rs b/src/ui/timeline.rs
index b5635f5..2b7eb33 100644
--- a/src/ui/timeline.rs
+++ b/src/ui/timeline.rs
@@ -1,9 +1,11 @@
 use crate::audio::sources::metronome;
 use crate::audio::sources::synthesizer;
 use crate::utils::chrono;
+use gtk::glib::{ControlFlow, Propagation};
 use gtk::prelude::*;
 use std::collections::HashMap;
 use std::sync::{Arc, Mutex};
+use gtk::Adjustment;
 
 // -- Constants ---------------------------------------------------------------
 const SECOND: i32 = 1000;
@@ -32,7 +34,7 @@ pub struct Timeline {
 
 impl Timeline {
     pub fn new(notes_number: usize, metronome: metronome::Handle) -> Arc<Self> {
-        let scrolled_window = gtk::ScrolledWindow::new(gtk::NONE_ADJUSTMENT, gtk::NONE_ADJUSTMENT);
+        let scrolled_window = gtk::ScrolledWindow::new(None::<&Adjustment>, None::<&Adjustment>);
         let drawing_area = gtk::DrawingArea::new();
         drawing_area.set_size_request(-1, notes_number as i32 * BAR_HEIGHT as i32);
         scrolled_window.add(&drawing_area);
@@ -49,12 +51,12 @@ impl Timeline {
         let timeline_ = timeline.clone();
         drawing_area.connect_draw(move |_, ctx| {
             timeline_.draw(ctx);
-            Inhibit(false)
+            Propagation::Proceed
         });
 
         drawing_area.add_tick_callback(|wgt, _| {
             wgt.queue_draw();
-            Continue(true)
+            ControlFlow::Continue
         });
 
         timeline
@@ -118,18 +120,18 @@ impl Timeline {
 
         // Select font
         ctx.select_font_face("Sans", cairo::FontSlant::Normal, cairo::FontWeight::Normal);
-        ctx.set_font_matrix(cairo::Matrix {
-            xx: FONT_SIZE * pxw,
-            yy: FONT_SIZE * pxh,
-            x0: 0.0,
-            y0: 0.0,
-            xy: 0.0,
-            yx: 0.0,
-        });
+        ctx.set_font_matrix(cairo::Matrix::new(
+            FONT_SIZE * pxw,
+            FONT_SIZE * pxh,
+            0.0,
+            0.0,
+            0.0,
+            0.0,
+        ));
 
         // Draw plot background
         let color = gradation(bg, fg, 0.1);
-        ctx.set_source_rgb(color.red, color.green, color.blue);
+        ctx.set_source_rgb(color.red(), color.green(), color.blue());
         for i in (0..self.notes_number).step_by(2) {
             let y = 1.0 - (i + 1) as f64 * bar_height;
             ctx.rectangle(0.0, y, 1.0, bar_height);
@@ -138,7 +140,7 @@ impl Timeline {
 
         // Draw octave separators
         let color = gradation(bg, fg, 0.3);
-        ctx.set_source_rgb(color.red, color.green, color.blue);
+        ctx.set_source_rgb(color.red(), color.green(), color.blue());
         ctx.set_line_width(1.0 * pxh);
         let mut i = synthesizer::utils::get_c_next_octave(0);
         while i < self.notes_number as usize {
@@ -151,10 +153,10 @@ impl Timeline {
 
         // Draw octave headers
         let color = gradation(bg, fg, 0.3);
-        ctx.set_source_rgb(color.red, color.green, color.blue);
+        ctx.set_source_rgb(color.red(), color.green(), color.blue());
         ctx.rectangle(0.0, 0.0, header_octave_width, 1.0);
         let _ = ctx.fill();
-        ctx.set_source_rgb(fg.red, fg.green, fg.blue);
+        ctx.set_source_rgb(fg.red(), fg.green(), fg.blue());
         let mut i = 0;
         while i < self.notes_number {
             let y = 1.0 - i as f64 * bar_height;
@@ -164,10 +166,10 @@ impl Timeline {
         }
 
         // Draw note headers
-        ctx.set_source_rgba(fg.red, fg.green, fg.blue, 0.1);
+        ctx.set_source_rgba(fg.red(), fg.green(), fg.blue(), 0.1);
         ctx.rectangle(header_octave_width, 0.0, header_note_width, 1.0);
         let _ = ctx.fill();
-        ctx.set_source_rgb(fg.red, fg.green, fg.blue);
+        ctx.set_source_rgb(fg.red(), fg.green(), fg.blue());
         for i in 0..self.notes_number {
             let y = 1.0 - i as f64 * bar_height;
             ctx.move_to(header_octave_width + 3.0 * pxw, y - 3.0 * pxh);
@@ -180,7 +182,7 @@ impl Timeline {
         let beat_timestamp = self.metronome.get_timestamp() as i32;
 
         let color = gradation(bg, fg, 0.3);
-        ctx.set_source_rgb(color.red, color.green, color.blue);
+        ctx.set_source_rgb(color.red(), color.green(), color.blue());
         let small_period = MINUTE / bpm;
         let big_period = small_period * meter_numerator as i32;
         let start = view_duration % small_period + small_period
@@ -208,7 +210,7 @@ impl Timeline {
             let mut x_last = header_width;
             for entry in log.iter() {
                 let color = gradation(bg, fg, 0.7);
-                ctx.set_source_rgb(color.red, color.green, color.blue);
+                ctx.set_source_rgb(color.red(), color.green(), color.blue());
                 let start = entry.start;
                 let end = if entry.end == 0 { view_end } else { entry.end };
                 if end > view_start {
@@ -232,10 +234,10 @@ impl Timeline {
 }
 
 fn gradation(c1: gdk::RGBA, c2: gdk::RGBA, ratio: f64) -> gdk::RGBA {
-    gdk::RGBA {
-        red: c1.red + (c2.red - c1.red) * ratio,
-        green: c1.green + (c2.green - c1.green) * ratio,
-        blue: c1.blue + (c2.blue - c1.blue) * ratio,
-        alpha: c1.alpha + (c2.alpha - c1.alpha) * ratio,
-    }
+    gdk::RGBA::new(
+        c1.red() + (c2.red() - c1.red()) * ratio,
+        c1.green() + (c2.green() - c1.green()) * ratio,
+        c1.blue() + (c2.blue() - c1.blue()) * ratio,
+        c1.alpha() + (c2.alpha() - c1.alpha()) * ratio,
+    )
 }
